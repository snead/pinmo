/*
 * Pinmo
 * Startup
 *
*/

var electron = require('electron');
var express = require('express');
var app = express();
var Process = electron.app;
var browserWindow = electron.BrowserWindow;
var mainWindow = null;
    
Process.on('window-all-closed', function() {
    if(process.platform != 'darwin') {
        Process.quit();
    }
});

app.get('/', function (req, res) {
    res.send('hola');
});

app.listen(3333, function () {
    Process.on('ready', function() {

        mainWindow = new browserWindow({ width: 800, height: 600, title: 'Pinmo' });
        mainWindow.loadURL('http://localhost:3333');
        mainWindow.webContents.openDevTools();

        mainWindow.on('closed', function() {
            mainWindow = null;
            app = null;
      });
    });
})


